﻿using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;
using GoogleARCore.Examples.Common;

public class SurfaceDetectionController : MonoBehaviour
{

    public GameObject trackedPlanePrefab;

    private List<DetectedPlane> newPlanes = new List<DetectedPlane>();
    private bool isQuitting = false;

    public void Update()
    {
        Session.GetTrackables<DetectedPlane>(newPlanes, TrackableQueryFilter.New);
        for (int i = 0; i < newPlanes.Count; i++)
        {
            GameObject planeObject = Instantiate(trackedPlanePrefab,
                Vector3.zero,
                Quaternion.identity,
                transform);
            planeObject.GetComponent<DetectedPlaneVisualizer>()
                .Initialize(newPlanes[i]);
        }
    }
}