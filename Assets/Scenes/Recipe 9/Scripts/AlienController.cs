﻿using UnityEngine;

public class AlienController : MonoBehaviour
{

    public GameObject particleSystemPrefab;
    public AudioSource coinAudioSource;
    public AudioSource deathAudioSource;
    public GameUIController gameUIController;
    public CoinsManager coinsManager;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Coin")
        {
            GameObject particleGO = Instantiate(particleSystemPrefab, other.transform.position, particleSystemPrefab.transform.rotation);
            ParticleSystem ps = particleGO.GetComponent<ParticleSystem>();
            ps.Play();
            coinAudioSource.Play();
            gameUIController.IncreaseScore();
            Destroy(other.gameObject);
            Destroy(particleGO, ps.duration);
            coinsManager.AddNewCoin();
        }
        else if (other.tag == "Enemy")
        {
            deathAudioSource.Play();
            GameObject particleGO = Instantiate(particleSystemPrefab, other.transform.position, particleSystemPrefab.transform.rotation);
            ParticleSystem ps = particleGO.GetComponent<ParticleSystem>();
            var main = ps.main;
            main.startColor = Color.red;
            ps.Play();
            RootGameController.instance.GameOver();
            Destroy(particleGO, ps.duration);
        }
    }
}