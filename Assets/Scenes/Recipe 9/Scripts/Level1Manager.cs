﻿using UnityEngine;
using GoogleARCore;
using UnityEngine.UI;

public class Level1Manager : MonoBehaviour
{

    public GameObject playerAlien;
    public GameUIController gameUIController;

    private AlienMovement alienMovement;
    private EnemiesManager enemiesManager;
    private CoinsManager coinsManager;

    void Start()
    {
        enemiesManager = GetComponent<EnemiesManager>();
        coinsManager = GetComponent<CoinsManager>();
        alienMovement = playerAlien.GetComponent<AlienMovement>();
        playerAlien.SetActive(false);
    }

    private void StartGame()
    {
        gameUIController.StartGame();
        playerAlien.SetActive(true);
        alienMovement.StartMovement();
        coinsManager.PlaceCoins();
        enemiesManager.StartEnemies();
    }

    public void GameOver()
    {
        gameUIController.EndGame();
        enemiesManager.RemoveEnemies();
        coinsManager.RemoveCoins();
        alienMovement.StopMovement();
        playerAlien.SetActive(false);
    }

    public void HandlePlayButtonClicked()
    {
        playerAlien.transform.position =
            RootGameController.instance.gameAnchor.transform.position;
        StartGame();
    }
}