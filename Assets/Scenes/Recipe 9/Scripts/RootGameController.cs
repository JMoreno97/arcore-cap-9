﻿using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using GoogleARCore;
using GoogleARCore.Examples.Common;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RootGameController : MonoBehaviour
{
    public static RootGameController instance = null;
    public GameObject searchingCanvas;

    [System.NonSerialized]
    public DetectedPlane detectedPlane;
    [System.NonSerialized]
    public Anchor gameAnchor;
    [System.NonSerialized]
    public Vector2 maxPos;
    [System.NonSerialized]
    public Vector2 minPos;

    private const string LEVEL_NAME_01 = "LevelScene";
    private bool firstTime = true;
    private GameObject currentLevel;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void Update()
    {
        if (Input.touchCount > 0 && firstTime)
        {
            Touch touch = Input.GetTouch(0);
            TrackableHit hit;
            TrackableHitFlags raycastFilter =
                TrackableHitFlags.PlaneWithinPolygon;

            if (Frame.Raycast(touch.position.x,
                    touch.position.y,
                    raycastFilter,
                    out hit))
            {
                firstTime = false;
                gameAnchor = hit.Trackable.CreateAnchor(hit.Pose);
                detectedPlane = (DetectedPlane)hit.Trackable;
                StartCoroutine(LoadLevel(LEVEL_NAME_01));
            }
        }
    }

    IEnumerator LoadLevel(string sceneName)
    {
        searchingCanvas.SetActive(false);
        AsyncOperation asyncOp =
            SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        asyncOp.allowSceneActivation = false;

        while (!asyncOp.isDone)
        {
            // Load is complete at 0.9
            if (asyncOp.progress == 0.9f)
            {
                // Scene loaded, now activate
                asyncOp.allowSceneActivation = true;
                SetupGame();
            }
            yield return null;
        }
    }

    private void SetupGame()
    {
        float centerOffsetX = detectedPlane.ExtentX / 2f;
        float centerOffsetZ = detectedPlane.ExtentZ / 2f;
        float minX = detectedPlane.CenterPose.position.x - centerOffsetX;
        float minZ = detectedPlane.CenterPose.position.z - centerOffsetZ;
        float maxX = detectedPlane.CenterPose.position.x + centerOffsetX;
        float maxZ = detectedPlane.CenterPose.position.z + centerOffsetZ;
        minPos = new Vector2(minX, maxZ);
        maxPos = new Vector2(maxX, minZ);

        HidePlanes();
        currentLevel = GameObject.FindGameObjectWithTag("Level1");
        currentLevel.transform.position = gameAnchor.transform.position;
        currentLevel.transform.SetParent(gameAnchor.transform);
    }

    private void HidePlanes()
    {
        GameObject[] planes =
            GameObject.FindGameObjectsWithTag("DetectedPlane");
        foreach (GameObject plane in planes)
        {
            plane.SetActive(false);
        }
    }

    public void GameOver()
    {
        currentLevel = GameObject.FindGameObjectWithTag("Level1");
        Level1Manager levelManager = currentLevel.GetComponent<Level1Manager>();
        levelManager.GameOver();
    }
}