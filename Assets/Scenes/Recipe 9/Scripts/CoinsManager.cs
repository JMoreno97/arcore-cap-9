﻿using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;
using GoogleARCore.Examples.HelloAR;

public class CoinsManager : MonoBehaviour
{

    public GameObject coinPrefab;

    private const int numCoins = 30;
    private List<GameObject> coins;
    private Vector2 maxPos;
    private Vector2 minPos;
    private DetectedPlane detectedPlane;

    public void PlaceCoins()
    {
        detectedPlane = RootGameController.instance.detectedPlane;
        maxPos = RootGameController.instance.maxPos;
        minPos = RootGameController.instance.minPos;

        coins = new List<GameObject>();
        for (int i = 0; i < numCoins; i++)
        {
            AddNewCoin();
        }
    }

    public void AddNewCoin()
    {
        float randX = Random.Range(minPos.x, maxPos.x);
        float yPos = detectedPlane.CenterPose.position.y;
        float randZ = Random.Range(minPos.y, maxPos.y);
        Vector3 randPos = new Vector3(randX, yPos, randZ);
        GameObject coin =
            Instantiate(coinPrefab, randPos, coinPrefab.transform.rotation);
        coin.transform.SetParent(
            RootGameController.instance.gameAnchor.transform);
        coins.Add(coin);
    }

    public void RemoveCoins()
    {
        foreach (GameObject coin in coins)
        {
            Destroy(coin);
        }
        coins.Clear();
    }
}