﻿using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;
using GoogleARCore.Examples.HelloAR;

public class EnemiesManager : MonoBehaviour
{

    public GameObject enemyPrefab01;

    private List<GameObject> enemies;
    private const int numEnemies = 15;

    void Start()
    {
        enemies = new List<GameObject>();
    }

    public void StartEnemies()
    {

        Vector2 maxPos = RootGameController.instance.maxPos;
        Vector2 minPos = RootGameController.instance.minPos;
        float yPos =
            RootGameController.instance.detectedPlane.CenterPose.position.y;

        for (int i = 0; i < numEnemies; i++)
        {
            Vector3 randPos =
                new Vector3(Random.Range(minPos.x, maxPos.x),
                    yPos,
                    Random.Range(minPos.y, maxPos.y));
            GameObject enemy =
                Instantiate(enemyPrefab01,
                    randPos,
                    enemyPrefab01.transform.rotation);
            enemy.transform.SetParent(
                RootGameController.instance.gameAnchor.transform);
            enemies.Add(enemy);
            EnemyController enemyController =
                enemy.GetComponent<EnemyController>();
            enemyController.StartEnemy();
        }
    }

    public void RemoveEnemies()
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            GameObject enemy = enemies[i];
            EnemyController enemyController =
                enemy.GetComponent<EnemyController>();
            enemyController.KillEnemy();
            Destroy(enemy);
        }
        enemies.Clear();
    }
}