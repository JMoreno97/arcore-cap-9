﻿using UnityEngine;

public class AlienMovement : MonoBehaviour
{

    private bool leftBtnIsDown = false;
    private bool rightBtnIsDown = false;
    private bool inGame = false;
    private const float speed = 0.2f;
    private const float rotSpeed = 110f;

    void Update()
    {
        if (leftBtnIsDown)
        {
            transform.Rotate(Vector3.up * Time.deltaTime * -rotSpeed, Space.World);
        }

        if (rightBtnIsDown)
        {
            transform.Rotate(Vector3.up * Time.deltaTime * rotSpeed, Space.World);
        }

        if (inGame)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * speed);
        }
    }

    public void StartMovement()
    {
        ResetMovement();
        inGame = true;
    }

    public void StopMovement()
    {
        inGame = false;
    }

    public void HandleLeftButtonDown()
    {
        leftBtnIsDown = true;
    }

    public void HandleLeftButtonUp()
    {
        leftBtnIsDown = false;
    }

    public void HandleRightButtonDown()
    {
        rightBtnIsDown = true;
    }

    public void HandleRightButtonUp()
    {
        rightBtnIsDown = false;
    }

    public void ResetMovement()
    {
        leftBtnIsDown = false;
        rightBtnIsDown = false;
    }
}