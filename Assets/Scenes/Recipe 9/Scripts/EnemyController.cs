﻿using UnityEngine;
using DG.Tweening;

public class EnemyController : MonoBehaviour
{

    public void StartEnemy()
    {
        AnimateToNewLocation();
    }

    private void AnimateToNewLocation()
    {

        Vector2 maxPos = RootGameController.instance.maxPos;
        Vector2 minPos = RootGameController.instance.minPos;

        Vector3 randPos = new Vector3(Random.Range(minPos.x, maxPos.x), transform.position.y, Random.Range(minPos.y, maxPos.y));
        float distance = Vector3.Distance(randPos, transform.position);
        transform.DOMove(randPos, 10f * distance, false).OnComplete(AnimateToNewLocation);
        Vector3 direction = (randPos - transform.position).normalized;
        transform.rotation = Quaternion.LookRotation(direction);
    }

    public void KillEnemy()
    {
        transform.DOKill(false);
    }
}