﻿using UnityEngine;
using UnityEngine.UI;

public class GameUIController : MonoBehaviour
{

    public GameObject leftButton;
    public GameObject rightButton;
    public GameObject playButton;
    public Text scoreText;
    public Text timerText;

    private bool inGame = false;
    private float elapsedTime;
    private int currentScore;

    void Start()
    {
        leftButton.SetActive(false);
        rightButton.SetActive(false);
        playButton.SetActive(true);
    }

    void Update()
    {
        if (inGame)
        {
            UpdateTimer();
        }
    }

    public void StartGame()
    {
        elapsedTime = 0f;
        currentScore = 0;
        leftButton.SetActive(true);
        rightButton.SetActive(true);
        playButton.SetActive(false);
        inGame = true;
    }

    public void EndGame()
    {
        inGame = false;
        playButton.SetActive(true);
        leftButton.SetActive(false);
        rightButton.SetActive(false);
    }

    public void IncreaseScore()
    {
        currentScore++;
        scoreText.text = currentScore.ToString();
    }

    private void UpdateTimer()
    {
        elapsedTime += Time.deltaTime;
        int minutes = Mathf.FloorToInt(elapsedTime / 60F);
        int seconds = Mathf.FloorToInt(elapsedTime - minutes * 60);
        string formattedTime = string.Format("{0:0}:{1:00}", minutes, seconds);
        timerText.text = formattedTime;
    }
}